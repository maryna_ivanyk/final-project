<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <body>
        <c:forEach var="service" items="${sessionScope.services}">
            ${service.name}
            <br>
            <hr>
            <c:forEach var="tariff" items="${sessionScope.tariffs}">
                ${tariff.name} ${tariff.description} ${tariff.cost}
                <br>
            </c:forEach>
        </c:forEach>
    </body>
</html>